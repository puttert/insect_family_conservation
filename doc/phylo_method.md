Methods for visualising protection gap with phylogeny. 

The insecta family phylogeny displayed in figure X is obtained from timetree.org (Kumar et al. 2017) with "Group" specified as "insecta" and "Rank" specified as "family". 
The R package `ape` (Paradis & Schliep 2019) was used to imported the tree (`read.tree`) and pruned with `drop.tip` to only represent insect families where protection gap data is available. A customised script (***say somethinga abut data/code availability or include the bitbucket repository***) was then used to colour code different nodes based on the protection gap values. The final phylogeny with colour coded data is generated using the function `ggtree` in `ggtree` package (Yu et al. 2017, 2018, and Yu 2020). 




Version 2
The insecta family phylogeny displayed in figure X is obtained from timetree.org (Kumar et al. 2017) with "Group" specified as "insecta" and "Rank" specified as "family". The R package `ape` (Paradis & Schliep 2019) was used to import the tree (`read.tree`) and pruned with `drop.tip` to only represent insect families where percentages of coverage data is available. A customised script (***say somethinga abut data/code availability or include the bitbucket repository***) was then used to colour code different nodes based on the protection coverage values. The values represent how much of the distribution of the family is found in protected areas. The final phylogeny with colour coded data is generated using the function `ggtree` in `ggtree` package (Yu et al. 2017, 2018, and Yu 2020). 


Bibliography:

S. Kumar, G. Stecher, M. Suleski, and S.B. Hedges, 2017. TimeTree: a resource for timelines, timetrees, and divergence times. Molecular Biology and Evolution 34: 1812-1819, DOI: 10.1093/molbev/msx116. 

Paradis E, Schliep K (2019). “ape 5.0: an environment for modern phylogenetics and evolutionary analyses in R.” Bioinformatics, 35, 526-528. 

Guangchuang Yu. Using ggtree to visualize data on tree-like structures. Current Protocols in Bioinformatics, 2020, 69:e96.
  doi: 10.1002/cpbi.96

Guangchuang Yu, Tommy Tsan-Yuk Lam, Huachen Zhu, Yi Guan. Two methods for mapping and visualizing associated data on
  phylogeny using ggtree. Molecular Biology and Evolution 2018, 35(2):3041-3043. doi: 10.1093/molbev/msy194

Guangchuang Yu, David Smith, Huachen Zhu, Yi Guan, Tommy Tsan-Yuk Lam. ggtree: an R package for visualization and
  annotation of phylogenetic trees with their covariates and other associated data. Methods in Ecology and Evolution 2017,
  8(1):28-36. doi:10.1111/2041-210X.12628